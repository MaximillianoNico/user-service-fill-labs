package main

import (
	"log"

	config "gitlab.com/MaximillianoNico/user-service-fill-labs/app/config"
	webserver "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure"
	repository "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure/repository"
	"github.com/joho/godotenv"
)

func initializeStorate() (*repository.MongoInstance, error) {
	log.Printf("Connecting to Storage...")
	store := &repository.MongoInstance{}
	err := store.InitDatabase()

	if err != nil {
		log.Fatalf("Can't connect to Mongo Storage: %v", err)
	}

	return store, nil
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Printf("Error loading .env file")
	}

	// Connect to Database
	store, err := initializeStorate()

	if err != nil {
		log.Fatal("Failed Initialize DB")
	}

	App := webserver.NewApp(
		&config.AppConfig{
			Version: "1.0.0",
		},
		store,
	)

	App.RunServer()
}
