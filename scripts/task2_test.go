package main

import "testing"

func TestOperation(t *testing.T) {
	expect := []int{9, 4, 2}
	result := Operation()

	t.Logf("result : %v", result)
	t.Logf("expect : %v", expect)
	for idx, item := range result {
		if item != expect[idx] {
			t.Errorf("didn't match with expected sort array\n index: %v -> expect %v, actual %v", idx, expect[idx], item)
		}
	}
}
