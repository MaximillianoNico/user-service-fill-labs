package main

import (
	"fmt"
)

type Item struct {
	name  string
	count int
}

func findExist(items []Item, itm string) (*Item, int) {
	for idxExist, existItm := range items {
		// fmt.Printf("find items match: %v\n", items)
		// fmt.Printf("find exist: %v ~ check %v\n\n", existItm.name, itm)

		if existItm.name == itm {
			// fmt.Printf("next %v - %v\n", existItm.name, itm)
			return &Item{
				name:  existItm.name,
				count: existItm.count,
			}, idxExist
		}
	}

	return nil, -1
}

func findHighest(items []Item) Item {
	result := Item{
		name:  "",
		count: 0,
	}

	for _, itm := range items {
		if itm.count > result.count {
			result = itm
		}
	}

	return result
}

func findRepeatedData() Item {
	fmt.Printf("Task 2: Find Repeated Data \n")
	list := []string{"apple", "pie", "apple", "red", "red", "red"}
	items := []Item{}

	for idx, itm := range list {
		fmt.Printf("\nitm: %v\n", itm)
		if idx == 0 {
			fmt.Printf("INIT\n")
			items = append(items, Item{
				name:  itm,
				count: 1,
			})
		} else {
			// fmt.Printf("find items match: %v\n", items)
			fmt.Printf("==============================\n")
			itemMatch, idx := findExist(items, itm)

			if itemMatch == nil && idx == -1 {
				items = append(items, Item{
					name:  itm,
					count: 1,
				})
			} else {
				items[idx] = Item{
					name:  itemMatch.name,
					count: itemMatch.count + 1,
				}
			}
			fmt.Printf("==============================\n")
		}
	}

	result := findHighest(items)

	return result
}
