// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"strings"
)

type SortLibs interface {
	SortStringBasedOnNumberChar(arr []string) []string
}

func getTotalWord(word string, checkWord string) int {
	items := strings.Split(word, "")
	total := 0
	for _, s := range items {
		if s == checkWord {
			total += 1
		}
	}

	return total
}

func Reduce(arr []string, callback func(prev []string, args string) []string, init string) []string {
	main := []string{init}

	for _, item := range arr {
		main = callback(main, item)
	}

	return main
}

func Swap(arr []string, idx int) {
	temp := arr[idx]
	arr[idx] = arr[idx+1]
	arr[idx+1] = temp
}

func SortStringBasedOnNumberChar(arr []string) []string {
	aplhabet := strings.ToLower("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	sorting := Reduce(arr, func(prev []string, nextWord string) []string {
		if len(prev) == 1 && prev[0] == "" { // if initial data doesn't have any value, then return next item
			return []string{nextWord}
		}

		newArr := append([]string{nextWord}, prev...) // merge array

		for idx, item := range newArr {
			itemTotalWordA := getTotalWord(item, "a")

			if idx < len(newArr)-1 {

				totalNextWord := getTotalWord(newArr[idx+1], "a")
				isContainWordA := totalNextWord != 0 && itemTotalWordA != 0

				if itemTotalWordA < totalNextWord { // if total `a`word in next item more than current
					Swap(newArr, idx) // switch position array
				}

				if !isContainWordA { // detect if current and next item is doesn't contain `a` word
					lengthItem := len([]rune(item))
					lengthNextItem := len([]rune(newArr[idx+1]))

					if lengthNextItem == lengthItem && newArr[idx+1] != item {
						nextWordSplit := strings.Split(newArr[idx+1], "")
						itemWordSplit := strings.Split(item, "")

						/**
						* check position and do swap based on lowest number
						* ex: kf, ef
						* `kf` -> 11, `ef` -> 4 (should swap position `ef`` to the previous `kf``)
						 */
						if strings.Index(aplhabet, nextWordSplit[0]) < strings.Index(aplhabet, itemWordSplit[0]) { // check position alphabet numbers
							// swap position based on lowest alphabet number
							temp := newArr[idx]
							newArr[idx] = newArr[idx+1]
							newArr[idx+1] = temp
						}
					}

					if lengthNextItem > lengthItem {
						Swap(newArr, idx) // switch position array
					}
				}
			}
		}

		return newArr
	}, "")

	return sorting
}

func main() {
	fmt.Printf("Task 1: Sorting \n")
	arr := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	result := SortStringBasedOnNumberChar(arr)
	fmt.Println(result)
	fmt.Printf("=======END=======\n\n")

	resultOpt := Operation()
	fmt.Printf("task2 %v\n", resultOpt)

	resultFind := findRepeatedData()
	fmt.Printf("task3 %v\n", resultFind)
}
