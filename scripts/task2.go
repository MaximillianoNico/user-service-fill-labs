package main

import (
	"fmt"
	"math"
)

func findRank(n int) int {
	for i := 1; i < n; i++ {
		check := math.Pow(float64(i), 2)

		if int(check) == n {
			fmt.Printf("check: %v\n", n)
			return i
		}
	}

	return 0
}

func Operation() []int {
	n := 9
	fmt.Printf("Task 2: Recurcive \n")
	var recurcive func(n int) int
	items := []int{}

	recurcive = func(n int) int {
		if n == 1 {
			return n
		}

		fmt.Printf("%v\n", n)
		items = append(items, n)
		result := math.Floor(float64(n) / 2)
		return recurcive(int(result))

	}

	recurcive(n)
	fmt.Printf("=======END=======\n\n")

	return items
}
