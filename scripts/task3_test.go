package main

import "testing"

func TestFindHighest(t *testing.T) {
	expect := Item{name: "red", count: 3}
	result := findRepeatedData()

	t.Logf("result : %v", result)
	t.Logf("expect : %v", expect)
	if result.name != expect.name {
		if result.count != expect.count {
			t.Errorf("didn't match count with expected sort array\n expect %v, actual %v", expect, result)
		}
		t.Errorf("didn't match name with expected sort array\n expect %v, actual %v", expect, result)
	}

}
