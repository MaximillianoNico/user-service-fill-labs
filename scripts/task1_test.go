package main

import "testing"

func TestSorting(t *testing.T) {
	arr := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	expect := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"}
	result := SortStringBasedOnNumberChar(arr)

	t.Logf("result : %v", result)
	t.Logf("expect : %v", expect)
	for idx, item := range result {
		if item != expect[idx] {
			t.Errorf("didn't match with expected sort array\n index: %v -> expect %v, actual %v", idx, expect[idx], item)
		}
	}
}
