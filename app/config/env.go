package Config

type AppConfig struct {
	Version     string
	BindAddress string
}
