package applications

import "time"

type Application struct {
	authToken []byte
}

func NewApplication(jwtToken []byte) *Application {
	return &Application{
		authToken: jwtToken,
	}
}

type AuthApps interface {
	CreateToken(ttl time.Duration, payload interface{}) (string, error)
	Validate(token string) (interface{}, error)
}

type FinanceApps interface {
	GetReportMonthly()
	FilterInOut()
}

type Apps interface {
	AuthApps
	FinanceApps
}
