package applications

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func (app *Application) CreateToken(ttl time.Duration, payload interface{}) (string, error) {
	now := time.Now().UTC()

	claims := make(jwt.MapClaims)
	claims["dat"] = payload             // Our custom data.
	claims["exp"] = now.Add(ttl).Unix() // The expiration time after which the token must be disregarded.
	claims["iat"] = now.Unix()          // The time at which the token was issued.
	claims["nbf"] = now.Unix()          // The time before which the token must be disregarded.

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(app.authToken)
	if err != nil {
		return "", fmt.Errorf("create: %w", err)
	}

	return token, nil
}

func (app *Application) Validate(token string) (map[string]interface{}, error) {
	tokenValidate, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}

		return app.authToken, nil
	})
	if err != nil {
		return nil, fmt.Errorf("validate: %w", err)
	}

	claims, ok := tokenValidate.Claims.(jwt.MapClaims)
	if !ok || !tokenValidate.Valid {
		return nil, fmt.Errorf("validate: invalid")
	}

	return claims["dat"].(map[string]interface{}), nil
}
