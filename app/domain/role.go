package domain

type Role struct {
	ID       string `bson:"_id"`
	Name     string `bson:"name" json:"name"`
	Desc     string `bson:"desc" json:"desc"`
	CreateAt string `bson:"createAt" json:"createAt"`
}
