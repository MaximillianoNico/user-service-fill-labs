package domain

type Users struct {
	ID       string   `bson:"_id"`
	Name     string   `bson:"name" json:"name"`
	CreateAt string   `bson:"createAt" json:"createAt"`
	Email    string   `bson:"email" json:"email"`
	Password string   `bson:"password" json:"password"`
	IsActive bool     `bson:"isActive" json:"isActive"`
	Roles    []string `bson:"roles" json:"roles"`
}

type UserList struct {
	ID       string   `bson:"_id"`
	Name     string   `bson:"name" json:"name"`
	CreateAt string   `bson:"createAt" json:"createAt"`
	Email    string   `bson:"email" json:"email"`
	IsActive bool     `bson:"isActive" json:"isActive"`
	Roles    []string `bson:"roles" json:"roles"`
}

type SignInPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type SignaturePayload struct {
	Email string   `json:"email"`
	Name  string   `json:"name"`
	Roles []string `json:"roles"`
}
