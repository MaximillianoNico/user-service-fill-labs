package Interface

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	applications "gitlab.com/MaximillianoNico/user-service-fill-labs/app/applications"
	config "gitlab.com/MaximillianoNico/user-service-fill-labs/app/config"
	middleware "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure/middleware"
	repository "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure/repository"
	controllers "gitlab.com/MaximillianoNico/user-service-fill-labs/app/interface/controllers"
)

type APIRouter struct {
	config     *config.AppConfig
	app        *fiber.App
	repository repository.Repository
}

func NewRouter(
	config *config.AppConfig,
	app *fiber.App,
	repository repository.Repository,
) *APIRouter {
	return &APIRouter{
		config:     config,
		app:        app,
		repository: repository,
	}
}

func (r *APIRouter) Init() *fiber.App {
	apps := applications.NewApplication([]byte(os.Getenv("JWTSECRET")))
	middleware := middleware.JWTAuth(apps)
	ctr := controllers.NewController(r.config, r.repository, apps)

	// Monitoring
	r.app.Get("/dashboard", monitor.New())

	// Grouping Routing
	api := r.app.Group("/api-public", middleware.PublicRoute)
	api.Get("/health-check", ctr.HealthCheck)

	apiAuth := r.app.Group("/auth", middleware.PublicRoute)
	apiAuth.Post("/sign-in", ctr.SignIn)
	apiAuth.Post("/registration", ctr.Registration)

	apiProtected := r.app.Group("/api", middleware.PrivateRoute)
	apiProtected.Get("/users", ctr.GetUsers)
	apiProtected.Get("/user/:id", ctr.GetUserById)
	apiProtected.Post("/user/new", ctr.CreateUser)
	apiProtected.Put("/user/:id", ctr.UpdateUserById)
	apiProtected.Delete("/user/:id", ctr.DeleteUserById)

	apiProtected.Get("/roles", ctr.GetRoles)
	apiProtected.Post("/role", ctr.CreateRole)

	return r.app
}
