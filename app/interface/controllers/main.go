package controllers

import (
	applications "gitlab.com/MaximillianoNico/user-service-fill-labs/app/applications"
	config "gitlab.com/MaximillianoNico/user-service-fill-labs/app/config"
	repository "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure/repository"
)

type Controllers struct {
	config     *config.AppConfig
	repository repository.Repository
	apps       *applications.Application
}

func NewController(
	config *config.AppConfig,
	repository repository.Repository,
	apps *applications.Application,
) *Controllers {
	return &Controllers{
		config:     config,
		repository: repository,
		apps:       apps,
	}
}
