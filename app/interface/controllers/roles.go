package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
)

func (ctr *Controllers) CreateRole(c *fiber.Ctx) error {
	payload := new(domain.Role)
	if err := c.BodyParser(payload); err != nil {
		return err
	}

	if payload.Name == "" || payload.Desc == "" {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "Name or Desc is required",
		})
	}

	roleExist, err := ctr.repository.FindRoleExists(payload.Name)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	if roleExist.Name != "" {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "role already exists",
		})
	}

	newRole, err := ctr.repository.CreateRole(payload)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"roleId":  newRole.ID,
		"Message": "SUCCESS",
	})
}

func (ctr *Controllers) GetRoles(c *fiber.Ctx) error {
	roles, err := ctr.repository.GetRoles()
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"data":    roles,
		"Message": "SUCCESS",
	})
}
