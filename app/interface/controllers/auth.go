package controllers

import (
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
	helper "gitlab.com/MaximillianoNico/user-service-fill-labs/app/helpers"
)

type SignInPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (ctr *Controllers) SignIn(c *fiber.Ctx) error {
	payload := new(SignInPayload)

	if err := c.BodyParser(payload); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	if payload.Email == "" || payload.Password == "" {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "Email or Password is required",
		})
	}

	userExists, err := ctr.repository.FindUserExists(payload.Email)

	fmt.Printf("userExists.Password: %v", userExists)
	isValid := helper.CheckPasswordHash(payload.Password, userExists.Password)

	if !isValid {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "Invalid email or password",
		})
	}

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	fmt.Printf("roles %v\n", userExists)
	token, err := ctr.apps.CreateToken(time.Hour, &domain.SignaturePayload{
		Email: userExists.Email,
		Name:  userExists.Name,
		Roles: userExists.Roles,
	})

	return c.JSON(&fiber.Map{
		"status":  "SUCCESS",
		"message": "Success Sign In",
		"data": &map[string]interface{}{
			"token": token,
		},
	})
}

func (ctr *Controllers) Registration(c *fiber.Ctx) error {
	payload := new(domain.Users)

	fmt.Printf("ok %v", payload)

	if err := c.BodyParser(payload); err != nil {
		return err
	}

	if payload.Password == "" {
		return fiber.ErrBadRequest
	}

	hashPassword, err := helper.HashPassword(payload.Password)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	payload.Password = hashPassword

	userId, err := ctr.repository.CreateUser(payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"userId":  userId.ID,
		"Message": "SUCCESS",
	})
}
