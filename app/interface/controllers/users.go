package controllers

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
	helper "gitlab.com/MaximillianoNico/user-service-fill-labs/app/helpers"
)

func (ctr *Controllers) CreateUser(c *fiber.Ctx) error {
	payload := new(domain.Users)

	fmt.Printf("ok %v", payload)

	if err := c.BodyParser(payload); err != nil {
		return err
	}

	// enable with feature flag
	if len(payload.Roles) == 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": "Roles is required",
		})
	}

	isRolesExists, err := ctr.repository.FindMultipleRoleExists(payload.Roles)

	if !isRolesExists {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": "Roles is not register in the system",
		})
	}

	hashPassword, err := helper.HashPassword(payload.Password)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	payload.Password = hashPassword

	userId, err := ctr.repository.CreateUser(payload)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"userId":  userId.ID,
		"Message": "SUCCESS",
	})
}

func (ctr *Controllers) GetUsers(c *fiber.Ctx) error {
	users, err := ctr.repository.GetUsers()
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"Message": "SUCCESS",
		"data":    users,
	})
}

func (ctr *Controllers) GetUserById(c *fiber.Ctx) error {
	userId := c.Params("id")

	if userId == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": "Id is required",
		})
	}

	user, err := ctr.repository.GetUserByID(userId)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"Message": "SUCCESS",
		"data":    user,
	})
}

func (ctr *Controllers) UpdateUserById(c *fiber.Ctx) error {
	userId := c.Params("id")
	payload := new(domain.UserList)

	if err := c.BodyParser(payload); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	if userId == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": "Id is required",
		})
	}
	user, err := ctr.repository.UpdateUser(payload, userId)

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"Message": "SUCCESS",
		"data":    user,
	})
}

func (ctr *Controllers) DeleteUserById(c *fiber.Ctx) error {
	userId := c.Params("id")
	if userId == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": "Id is required",
		})
	}

	isSuccess, err := ctr.repository.DeleteUser(userId)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"status":  "ERROR",
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"status":  "OK",
		"Message": "SUCCESS",
		"data":    isSuccess,
	})
}
