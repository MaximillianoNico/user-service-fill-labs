package repository

import (
	"context"
	"errors"
	"time"

	domain "gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var RolesCollectionName = "roles"

func (m *MongoInstance) RolesCollection() *mongo.Collection {
	return m.Client.Database(DatabaseName).Collection(RolesCollectionName)
}

func (m *MongoInstance) CreateRole(newRole *domain.Role) (*domain.Role, error) {
	var role = domain.Role{}
	err := m.UsersCollections().FindOne(
		context.TODO(),
		bson.M{
			"name": newRole.Name,
			"_id":  newRole.ID,
		},
	).Decode(&role)

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return nil, err
		}
	}

	if role.ID != "" {
		return nil, errors.New("role already exists")
	}

	result, err := m.RolesCollection().InsertOne(
		context.TODO(),
		bson.M{
			"name":     newRole.Name,
			"desc":     newRole.Desc,
			"createAt": time.Now().Format(time.RFC3339),
		},
	)

	if err != nil {
		return nil, err
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		return &domain.Role{
			ID:       oid.Hex(),
			Name:     role.Name,
			CreateAt: role.CreateAt,
			Desc:     role.Desc,
		}, nil
	}

	return nil, err
}

func (m *MongoInstance) GetRoles() ([]*domain.Role, error) {
	cursor, err := m.RolesCollection().Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}

	defer func() {
		cursor.Close(context.Background())
	}()

	var output []*domain.Role
	for cursor.Next(context.Background()) {
		var temp *domain.Role
		cursor.Decode(&temp)
		output = append(output, temp)
	}

	return output, nil
}

func (m *MongoInstance) FindMultipleRoleExists(ids []string) (bool, error) {
	oids := make([]primitive.ObjectID, len(ids))
	for i := range ids {
		objID, err := primitive.ObjectIDFromHex(ids[i])
		if err == nil {
			oids = append(oids, objID)
		}
	}
	query := bson.M{"_id": bson.M{"$in": oids}}

	cursor, err := m.RolesCollection().Find(context.Background(), query)
	if err != nil {
		return false, err
	}

	defer func() {
		cursor.Close(context.Background())
	}()

	var output []*domain.Role
	for cursor.Next(context.Background()) {
		var temp *domain.Role
		cursor.Decode(&temp)
		output = append(output, temp)
	}

	if len(output) != len(ids) {
		return false, nil
	}

	return true, nil
}

func (m *MongoInstance) FindRoleExists(name string) (*domain.Role, error) {
	var findRole = domain.Role{}
	err := m.RolesCollection().FindOne(
		context.TODO(),
		bson.M{
			"name": name,
		},
	).Decode(&findRole)

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return nil, err
		}
	}

	return &domain.Role{
		ID:       findRole.ID,
		Name:     findRole.Name,
		CreateAt: findRole.CreateAt,
		Desc:     findRole.Desc,
	}, nil
}
