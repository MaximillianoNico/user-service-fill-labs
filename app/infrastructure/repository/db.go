package repository

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	domain "gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
	helper "gitlab.com/MaximillianoNico/user-service-fill-labs/app/helpers"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoInstance struct {
	Client *mongo.Client
	DB     *mongo.Database
}

var DatabaseName = "byFood"

func (c *MongoInstance) InitDatabase() error {
	if os.Getenv("APP_ENV") != "production" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	credential := options.Credential{
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
	}

	conf := options.Client().ApplyURI(os.Getenv("DB_URL"))
	if os.Getenv("APP_ENV") == "production" {
		conf = options.Client().ApplyURI(
			os.Getenv("DB_URL"),
		).SetAuth(credential)
	}

	client, err := mongo.NewClient(conf)

	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)

		return err
	}

	fmt.Println("Database connected!")

	c.Client = client
	c.DB = client.Database(os.Getenv("DB"))

	// Do Initial User Account
	ClientDB := c.Client.Database("byFood")

	var findRole = domain.Role{}
	errFind := ClientDB.Collection("roles").FindOne(
		context.TODO(),
		bson.M{
			"name": "superadmin",
		},
	).Decode(&findRole)

	if errFind != nil {
		if errFind == mongo.ErrNoDocuments {
			// return nil, err

			result, err := ClientDB.Collection("roles").InsertOne(
				context.TODO(),
				bson.M{
					"name":     "superadmin",
					"desc":     "superadmin",
					"createAt": time.Now().Format(time.RFC3339),
				},
			)

			if err != nil {
				fmt.Errorf("error: %v", err)
			}

			if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
				hashPassword, err := helper.HashPassword("admin123321")
				if err != nil {
					fmt.Errorf("error: %v", err)
				}
				ClientDB.Collection("users").InsertOne(
					context.TODO(),
					bson.M{
						"name":     "admin",
						"email":    "superadmin@test.com",
						"roles":    []string{oid.Hex()},
						"isActive": true,
						"createAt": time.Now().Format(time.RFC3339),
						"password": hashPassword,
					},
				)
			}

		}

		fmt.Errorf("error: %v", errFind)
	}

	return nil
}

type Repository interface {
	InitDatabase() error
	//
	GetUsers() ([]*domain.UserList, error)
	GetUserByID(id string) (*domain.UserList, error)
	CreateUser(*domain.Users) (*domain.Users, error)
	UpdateUser(updatedUser *domain.UserList, userId string) (*domain.UserList, error)
	FindUserExists(email string) (*domain.Users, error)
	DeleteUser(userId string) (bool, error)
	//
	CreateRole(newRole *domain.Role) (*domain.Role, error)
	FindRoleExists(uid string) (*domain.Role, error)
	FindMultipleRoleExists(ids []string) (bool, error)
	GetRoles() ([]*domain.Role, error)
}
