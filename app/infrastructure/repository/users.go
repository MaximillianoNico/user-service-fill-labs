package repository

import (
	"context"
	"errors"
	"time"

	domain "gitlab.com/MaximillianoNico/user-service-fill-labs/app/domain"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var UserCollection = "users"

func (m *MongoInstance) UsersCollections() *mongo.Collection {
	return m.Client.Database(DatabaseName).Collection(UserCollection)
}

func (m *MongoInstance) GetUsers() ([]*domain.UserList, error) {
	cursor, err := m.UsersCollections().Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}

	defer func() {
		cursor.Close(context.Background())
	}()

	var output []*domain.UserList
	for cursor.Next(context.Background()) {
		var temp *domain.UserList
		cursor.Decode(&temp)
		output = append(output, temp)
	}

	return output, nil
}

func (m *MongoInstance) CreateUser(newUser *domain.Users) (*domain.Users, error) {
	var user = domain.Users{}
	err := m.UsersCollections().FindOne(
		context.TODO(),
		bson.M{
			"email": newUser.Email,
		},
	).Decode(&user)

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return nil, err
		}
	}

	if user.ID != "" {
		return nil, errors.New("email already exists")
	}

	result, err := m.UsersCollections().InsertOne(
		context.TODO(),
		bson.M{
			"name":     newUser.Name,
			"email":    newUser.Email,
			"roles":    newUser.Roles,
			"isActive": newUser.IsActive,
			"createAt": time.Now().Format(time.RFC3339),
			"password": newUser.Password,
		},
	)

	if err != nil {
		return nil, err
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		return &domain.Users{
			ID:       oid.Hex(),
			Name:     user.Name,
			CreateAt: user.CreateAt,
			Email:    user.Email,
		}, nil
	}

	return nil, err
}

func (m *MongoInstance) UpdateUser(updatedUser *domain.UserList, userId string) (*domain.UserList, error) {
	objectId, errObj := primitive.ObjectIDFromHex(userId)
	if errObj != nil {
		return nil, errObj
	}

	if len(updatedUser.Roles) == 0 {
		return nil, errors.New("Roles can't be empty")
	}

	result := &domain.UserList{}
	err := m.UsersCollections().FindOneAndUpdate(
		context.TODO(),
		bson.M{
			"_id": objectId,
		}, // <- Find block
		bson.D{{
			"$set", bson.M{
				"name":     updatedUser.Name,
				"email":    updatedUser.Email,
				"isActive": updatedUser.IsActive,
				"roles":    updatedUser.Roles,
			},
		}},
		options.FindOneAndUpdate().SetReturnDocument(options.After), // <- Set option to return document after update (important)
	).Decode(&result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (m *MongoInstance) DeleteUser(userId string) (bool, error) {
	objectId, errObj := primitive.ObjectIDFromHex(userId)
	if errObj != nil {
		return false, errObj
	}

	result, err := m.UsersCollections().DeleteOne(context.TODO(), bson.M{"_id": objectId})
	if err != nil {
		return false, err
	}

	if result.DeletedCount == 0 {
		return false, errors.New("Failed delete item")
	}

	return true, nil
}

func (m *MongoInstance) GetUserByID(id string) (*domain.UserList, error) {
	objectId, errObj := primitive.ObjectIDFromHex(id)
	if errObj != nil {
		return nil, errObj
	}

	var findUser = domain.Users{}
	err := m.UsersCollections().FindOne(
		context.TODO(),
		bson.M{
			"_id": objectId,
		},
	).Decode(&findUser)

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return nil, err
		}
	}

	return &domain.UserList{
		ID:       findUser.ID,
		Name:     findUser.Name,
		CreateAt: findUser.CreateAt,
		Email:    findUser.Email,
		Roles:    findUser.Roles,
	}, nil
}

func (m *MongoInstance) FindUserExists(email string) (*domain.Users, error) {
	var findUser = domain.Users{}
	err := m.UsersCollections().FindOne(
		context.TODO(),
		bson.M{
			"email": email,
		},
	).Decode(&findUser)

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return nil, err
		}
	}

	return &domain.Users{
		ID:       findUser.ID,
		Name:     findUser.Name,
		CreateAt: findUser.CreateAt,
		Email:    findUser.Email,
		Roles:    findUser.Roles,
		Password: findUser.Password,
	}, nil
}
