package middleware

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	apps "gitlab.com/MaximillianoNico/user-service-fill-labs/app/applications"
)

type middleware struct {
	applications *apps.Application
}

func JWTAuth(application *apps.Application) *middleware {
	return &middleware{
		applications: application,
	}
}

func (m *middleware) PrivateRoute(c *fiber.Ctx) error {
	token := c.Request().Header.Peek("Authorization")

	split := strings.Split(string(token[:]), " ")

	if split[0] != "Bearer" {
		return c.Status(fiber.StatusForbidden).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "invalid authorization",
		})
	}

	userClaims, err := m.applications.Validate(string(split[1]))
	if err != nil {
		return c.Status(fiber.StatusForbidden).JSON(&fiber.Map{
			"status":  "ERROR",
			"message": "token is expired",
		})
	}

	email := userClaims["email"].(string)
	roles := userClaims["roles"].([]interface{})

	c.Locals("token", split[1])
	c.Locals("roles", roles)
	c.Locals("user_email", email)
	return c.Next()
}
