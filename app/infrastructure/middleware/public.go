package middleware

import (
	"github.com/gofiber/fiber/v2"
)

func (m *middleware) PublicRoute(c *fiber.Ctx) error {
	return c.Next()
}
