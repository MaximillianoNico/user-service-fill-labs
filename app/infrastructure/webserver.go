package webserver

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	config "gitlab.com/MaximillianoNico/user-service-fill-labs/app/config"
	repository "gitlab.com/MaximillianoNico/user-service-fill-labs/app/infrastructure/repository"
	router "gitlab.com/MaximillianoNico/user-service-fill-labs/app/interface"
)

type App struct {
	config     *config.AppConfig
	repository repository.Repository
}

func NewApp(
	config *config.AppConfig,
	store repository.Repository,
) *App {
	return &App{
		config:     config,
		repository: store,
	}
}

func (svc *App) RunServer() {
	app := fiber.New(fiber.Config{
		ServerHeader: "Fiber",
		AppName:      "Personal Finance Service v1.0.1",
	})

	app.Use(func(c *fiber.Ctx) error {
		return c.Next()
	})
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders: "",
	}))
	app.Use(logger.New())

	Router := router.NewRouter(
		svc.config,
		app,
		svc.repository,
	)

	app = Router.Init()

	port := ":3000"
	if os.Getenv("PORT") != "" {
		port = ":" + os.Getenv("PORT")
	}

	log.Printf("Starting user management server... address %v", port)
	app.Listen(port)
}
